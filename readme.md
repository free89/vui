### 项目介绍
一个Javascript界面库,基于vue.js实现.
 - 文件架构借鉴BUI,API借鉴easyui;
 - 文档使用jsduck生成;

项目特色:
 - 基于vue.js开发,控件由模板而定,维护简单,方便;
 - 代码简单,单个JS文件压缩后只有1~2KB;
 - 控件按需加载,用到哪个控件就加载哪个;
 - 文档采用jsduck生成,查阅方便;
 - 控件使用方式借鉴easyui,上手简单;
 

演示地址:[演示](http://durcframework.oschina.io/vui)
最佳实践:[vBack后台管理系统](http://git.oschina.net/durcframework/vBack)

QQ交流群:328180219,如有问题可以加群探讨,也可以在issues中留言.

![VUI](http://git.oschina.net/uploads/images/2016/0629/152607_c85ca3e0_332975.png "VUI")

![DOC](http://git.oschina.net/uploads/images/2016/0629/160120_fd95e5cb_332975.png "DOC")